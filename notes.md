jika ingin membuat version module
maka hanya perlu menambahkan tag pada saat kita push
dan biasakan jika membuat tag, di awalin dengan huruf v
ex :: v1.0.0
code ::
    git tag v1.0.0
    git push origin v1.0.0
jika push sudah berhasil
maka kita bisa lihat di github/lab
akan muncul tag version yang telah kita buat

jika ingin upgrade module yang kita buat
maka hanya perlu membuat tag baru ke git
